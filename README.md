# Dependencies
[Get ESP-IDF](https://docs.espressif.com/projects/esp-idf/en/stable/esp32/get-started/index.html#step-2-get-esp-idf)

# Compiling
```bash
git clone https://gitlab.com/VictorLamoine/esp_idf_examples.git
cd esp_idf_examples
get_idf # See https://docs.espressif.com/projects/esp-idf/en/latest/esp32/get-started/index.html#id5
idf.py build
```

Use `idf.py menuconfig` to configure the project if needed.
