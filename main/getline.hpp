#ifndef GET_LINE_HPP
#define GET_LINE_HPP

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <iostream>
#include <string>

static void getline(std::string &line,
                    const bool display_text = true,
                    const int max_len = 128)
{
  if (max_len < 0)
    return;

  char cline[max_len];
  int count(0);
  while (count < max_len)
  {
    int c = fgetc(stdin);
    if (c == '\n' || count == max_len - 1)
    {
      if (display_text)
        std::cout << "\r\n" << std::endl;
      cline[count] = '\0';
      break;
    }
    else if (c == '\b') // Backpace
    {
      if (count > 0)
        --count;

      cline[count] = '\0';
    }
    else if (c > 0 && c < (max_len - 1))
    {
      cline[count] = c;
      ++count;
    }

    if (display_text)
    {
      if (c != -1)
        std::cout << (char) c;
      if (c == '\b')
        std::cout << " \b";
    }

    vTaskDelay(pdMS_TO_TICKS(10));
  }

  line = cline;
}

#endif
