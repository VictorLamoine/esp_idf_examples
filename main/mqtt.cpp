#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include <iostream>
#include "sdkconfig.h"
#include <stdio.h>
#include "getline.hpp"

// Connect GPIO2 (LED) and GPIO4

using std::cout;
using std::cerr;
using std::endl;

#define BLINK_GPIO GPIO_NUM_2
#define INPUT_GPIO GPIO_NUM_4

static QueueHandle_t gpio_event_queue(nullptr);

static void task_led_blink(void *)
{
	while (1) {
		gpio_set_level(BLINK_GPIO, true);
		cout << "LED on" << endl;
		vTaskDelay(pdMS_TO_TICKS(1000));
		gpio_set_level(BLINK_GPIO, false);
		cout << "LED off" << endl;
		vTaskDelay(pdMS_TO_TICKS(1000));
	}
}

static void task_ask_message(void *)
{
	std::string name;
	while (1) {
		cout << "Enter a message: " << endl;
		getline(name);
		cout << "Message: " << name << endl;
	}
}

static void task_gpio_polling(void *)
{
	while (1) {
		cout << "GPIO[" << INPUT_GPIO << "] = " << std::boolalpha << gpio_get_level(INPUT_GPIO) << endl;
		vTaskDelay(pdMS_TO_TICKS(500));
	}
}

static void IRAM_ATTR gpio_isr_handler(void *arg)
{
	uint32_t gpio_num = (uint32_t) arg;
	xQueueSendFromISR(gpio_event_queue, &gpio_num, NULL);
}

static void task_gpio_intr(void *)
{
	gpio_num_t io_num;
	while (1) {
		if (xQueueReceive(gpio_event_queue, &io_num, portMAX_DELAY)) {
			cout << "GPIO[" << io_num << "] interrupt" << endl;
			vTaskDelay(pdMS_TO_TICKS(100)); // Debounce by blocking here
		}
	}
}

extern "C" void app_main()
{
	gpio_event_queue = xQueueCreate(1, sizeof(uint32_t)); // Debounce with short length

	esp_rom_gpio_pad_select_gpio(BLINK_GPIO);
	gpio_set_direction(BLINK_GPIO, GPIO_MODE_OUTPUT);

	gpio_config_t gpio_conf;
	gpio_conf.pin_bit_mask = 1ULL << INPUT_GPIO;
	gpio_conf.intr_type = GPIO_INTR_POSEDGE; // Positive edge = rising edge
	gpio_conf.mode = GPIO_MODE_INPUT;
	gpio_conf.pull_down_en = GPIO_PULLDOWN_ENABLE;
	gpio_conf.pull_up_en = GPIO_PULLUP_DISABLE;
	gpio_config(&gpio_conf);

	gpio_install_isr_service(ESP_INTR_FLAG_EDGE);
	gpio_isr_handler_add(INPUT_GPIO, gpio_isr_handler, (void *) INPUT_GPIO);

	xTaskCreate(task_led_blink, "led_blink", 2048, NULL, configMAX_PRIORITIES - 1, NULL);
	vTaskDelay(pdMS_TO_TICKS(10)); // Prevent text overlapping in cout
	xTaskCreate(task_ask_message, "ask_message", 2048, NULL, configMAX_PRIORITIES - 1, NULL);
	vTaskDelay(pdMS_TO_TICKS(10));
	xTaskCreate(task_gpio_polling, "gpio_polling", 2048, NULL, configMAX_PRIORITIES - 1, NULL);
	xTaskCreate(task_gpio_intr, "gpio_interrupt", 2048, NULL, configMAX_PRIORITIES - 1, NULL);
}
